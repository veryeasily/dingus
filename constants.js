export const BOT_ID = '2b01d2ae55212d23eb2de15e81'
export const DARKSKY_SECRET_KEY = process.env.DARKSKY_SECRET_KEY
export const GROUPME_API_PATH = 'https://api.groupme.com/v3/bots/post'
export const LOCATIONS = [
  {
    name: 'KC',
    lat: 39.076659,
    lon: -94.571056
  },
  {
    name: 'Lincoln',
    lat: 40.821122,
    lon: -96.701013
  },
  {
    name: 'New York',
    lat: 40.688263,
    lon: -73.980459
  },
  {
    name: 'Portland',
    lat: 45.526515,
    lon: -122.643777
  }
]
